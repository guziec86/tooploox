//
// Created by mat on 14.08.18.
//

#pragma once
#include "DiscreteFilter.h"
#include "Image.hpp"

namespace {
    constexpr int neighbourhoodhoodSquareSize = 3;

    std::vector<int> laplaceKernel = {1, 1, 1,
                                      1, -8, 1,
                                      1, 1, 1};

    std::vector<int> laplaceKernel2 = {0, 1, 0,
                                       1, -4, 1,
                                       0, 1, 0};

    std::vector<int> gaussianKernel = {1, 2, 1,
                                       2, 4, 2,
                                       1, 2, 1};

    uint intensityThreshold = 40;

}

class ImageProcessor{

public:

    Image PerformGaussBlurFiltering(const Image &sourceImage);

    Image PerformLaplacianFiltering(const Image &sourceImage);

    using PictureNumberWithPixelValueArray = std::vector<std::vector<std::pair<uint,int>>>;
    using PictureNumberWithPixelValueArrayRow = std::vector<std::pair<uint,int>>;

    PictureNumberWithPixelValueArray
    CreateSharpMapFromGroupOfImages(const std::vector<Image>& originalImages);

    Image GetLaplaceImageFromOriginal(Image originalImage);

    void FindGreaterValueOfGradient(const Image& processedImage,
                                    PictureNumberWithPixelValueArray& PictureNumberWithPixelValueArray,
                                    uint pictureNumber);

    Image CreateColourImageBasedOnMap(const std::vector<Image>& originalImages,
                                const PictureNumberWithPixelValueArray& table);

    Image CreateDepthImageBasedOnMap(const std::vector<Image>& originalImages,
                                     const PictureNumberWithPixelValueArray& table);

private:
};