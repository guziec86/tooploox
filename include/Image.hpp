//
// Created by mat on 01.08.18.
//

#pragma once

#include <opencv2/opencv.hpp>
#include <iostream>
#include "Colour.h"

namespace {

    constexpr float redToGray = 0.299;
    constexpr float greenToGray = 0.587;
    constexpr float blueToGray = 0.144;
}


// class responsible for all operations concerning Images
class Image{

public:
    Image() = default;
    Image(uint rowsNumber, uint columnsNumber);
    Image(cv::Mat sourceImage);

    Image(const Image &inputImage);

    // displays image in window with given win_name
    void displayImage(std::string const &windowName);

    void saveImage(std::string const &windowName);

    void TransformToGrey();

    std::vector<uchar> getPixelNeighbourhood(int rowNumber, int columnNumber) const;
    std::vector<cv::Vec3b> getPixelNeighbourhoodVec3b(int rowNumber, int columnNumber) const;
    std::vector<Colour> getPixelNeighbourhoodColour(int rowNumber, int columnNumber) const;

    uint getNumberOfRows() const{
        return numberOfRows;
    }

    uint getNumberOfColumns() const{
        return numberOfColumns;
    }

    void convertToAbs();

    cv::Mat convertToCVMat();
    cv::Mat convertToCVMatGrey();

    Colour operator()(uint i, uint j) const{
        return rawImage[i][j];
    }

    Colour& operator()(uint i, uint j) {
        return rawImage[i][j];
    }

    void setImageIsGrey(){
        imageIsGrey = true;
    }

    void clearImageIsGrey(){
        imageIsGrey = false;
    }

    bool isImageIsGrey() const{
        return imageIsGrey;
    }

private:
    using DynamicImage = std::vector<std::vector<Colour>>;

    DynamicImage rawImage;

    uint numberOfRows;
    uint numberOfColumns;
    bool imageIsGrey;

    bool dealWithBorderPixels(int rowIndex, int colIndex, Colour& value) const;
};