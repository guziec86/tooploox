//
// Created by mat on 14.08.18.
//

#pragma once

struct Colour{

    Colour() = default;

    int operator[](int i) const{
        switch (i){
            case 0: return red;
            case 1: return green;
            case 2: return blue;
        }
    }

    int& operator[](int i){
        switch (i){
            case 0: return red;
            case 1: return green;
            case 2: return blue;
        }
    }

    int red, green, blue;
};