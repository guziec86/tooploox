//
// Created by mat on 01.08.18.
//
#pragma once

#include <cstdint>
#include <sys/types.h>
#include <vector>
#include "Colour.h"


class DiscreteFilter {

public:
    DiscreteFilter() = default;
    DiscreteFilter(std::vector<int> kernel, float coefficient) : filterKernel(kernel), coefficient(coefficient)
    {
    };

    ~DiscreteFilter() = default;

    int getFilterElement(uint coefficientIndex) const {
        return filterKernel[coefficientIndex];
    }

    Colour operator* (const std::vector<Colour> partOfImage);

    uint size(){
        return filterKernel.size();
    }

private:

    std::vector<int> filterKernel;
    float coefficient;
};


