//
// Created by mat on 01.08.18.
//

#include "Image.hpp"
#include <cstdlib>

using namespace cv;

Image::Image(uint rowsNumber, uint columnsNumber)
{
    std::vector<Colour> imageRow;

    for (uint row = 0; row < rowsNumber; row++) {
        rawImage.push_back(imageRow);
        for (uint col = 0; col <  columnsNumber; col++) {
            rawImage[row].push_back(Colour());
        }
    }

    numberOfColumns = columnsNumber;
    numberOfRows = rowsNumber;
    imageIsGrey = false;
}

Image::Image(cv::Mat sourceImage)
        : numberOfRows(sourceImage.rows),
          numberOfColumns(sourceImage.cols)
{
    //resizing vector
    rawImage.resize( numberOfRows );
    for( std::vector<std::vector<Colour> >::iterator it = rawImage.begin();
         it != rawImage.end(); ++it)
    {
        it->resize( numberOfColumns );
    }

    for (uint i = 0; i < numberOfRows; i++){
        for (uint j = 0; j < numberOfColumns; j++){
            rawImage[i][j] = {static_cast<int>(sourceImage.at<Vec3b>(i,j).val[0]),
                              static_cast<int>(sourceImage.at<Vec3b>(i,j).val[1]),
                              static_cast<int>(sourceImage.at<Vec3b>(i,j).val[2])};
        }
    }
    imageIsGrey = false;
};

Image::Image(const Image &inputImage){
    rawImage = inputImage.rawImage;

    numberOfRows = inputImage.numberOfRows;
    numberOfColumns = inputImage.numberOfColumns;
}

void Image::displayImage(std::string const &windowName){
    if (isImageIsGrey())
    {
        cv::imshow( windowName, convertToCVMatGrey());
    }
    else
    {
        cv::imshow( windowName, convertToCVMat());
    }

    cv::waitKey(0);
}

void Image::saveImage(std::string const &fileName){
    if (isImageIsGrey())
    {
        cv::imwrite(fileName, convertToCVMatGrey());
    }
    else
    {
        cv::imwrite( fileName, convertToCVMat());
    }
}

void Image::TransformToGrey(){
    for (uint i = 0; i < numberOfRows; i++){
        for (uint j = 0; j < numberOfColumns; j++){
            Colour original = rawImage[i][j];
            // values for converting taken from
            // https://www.mathworks.com/help/matlab/ref/rgb2gray.html
            float newValue =
                    redToGray * static_cast<float>(original.red) +
                    greenToGray * static_cast<float>(original.green) +
                    blueToGray * static_cast<float>(original.blue);

            rawImage[i][j] = {static_cast<int>(newValue), 0, 0};
        }
    }
    setImageIsGrey();
}

void Image::convertToAbs() {
    for (uint i = 0; i < numberOfRows; i++) {
        for (uint j = 0; j < numberOfColumns; j++) {
            rawImage[i][j].red = std::abs(rawImage[i][j].red);
            rawImage[i][j].green = std::abs(rawImage[i][j].green);
            rawImage[i][j].blue = std::abs(rawImage[i][j].blue);
        }
    }
}

cv::Mat Image::convertToCVMat(){

    cv::Mat ouputImage(getNumberOfRows(), getNumberOfColumns(), CV_8UC3);
    for (uint i = 0; i < numberOfRows; i++){
        for (uint j = 0; j < numberOfColumns; j++){
            ouputImage.at<cv::Vec3b>(i,j).val[0] = static_cast<uchar>(rawImage[i][j].red);
            ouputImage.at<cv::Vec3b>(i,j).val[1] = static_cast<uchar>(rawImage[i][j].green);
            ouputImage.at<cv::Vec3b>(i,j).val[2] = static_cast<uchar>(rawImage[i][j].blue);
        }
    }

    return ouputImage;
}

cv::Mat Image::convertToCVMatGrey(){

    cv::Mat ouputImage(getNumberOfRows(), getNumberOfColumns(), CV_8UC1);
    for (uint i = 0; i < numberOfRows; i++){
        for (uint j = 0; j < numberOfColumns; j++){
            ouputImage.at<uchar>(i,j) = static_cast<uchar>(rawImage[i][j].red);
        }
    }

    return ouputImage;
}

std::vector<Colour> Image::getPixelNeighbourhoodColour(int rowNumber, int columnNumber) const{
    std::vector<Colour> vectorOfValues;
    for (int row = rowNumber-1; row <= rowNumber+1; row++){
        for (int column = columnNumber-1; column <= columnNumber+1; column++){
            Colour value;
            if (!dealWithBorderPixels(row, column, value)){
                value = rawImage[row][column];
            }
            vectorOfValues.push_back(value);
        }
    }
    return vectorOfValues;
}

bool Image::dealWithBorderPixels(int rowIndex, int colIndex, Colour& value) const{

    if ((rowIndex < 0) && (colIndex < 0)){
        value = rawImage[0][0];
        return true;
    }

    if ((rowIndex > numberOfRows-1) && (colIndex > numberOfColumns-1)){
        value = rawImage[numberOfRows-1][numberOfColumns-1];
        return true;
    }

    if (rowIndex < 0){
        value = rawImage[0][colIndex];
        return true;
    }

    if (colIndex < 0){
        value = rawImage[rowIndex][0];
        return true;
    }

    if (rowIndex > numberOfRows-1){
        value = rawImage[numberOfRows-1][colIndex];
        return true;
    }

    if (colIndex > numberOfColumns-1){
        value = rawImage[rowIndex][numberOfColumns-1];
        return true;
    }

    return false;
}