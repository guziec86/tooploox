//
// Created by mat on 01.08.18.
//

#include "DiscreteFilter.h"

#include "DiscreteFilter.h"
#include <stdexcept>

Colour DiscreteFilter::operator* (const std::vector<Colour> partOfImage){
    if (filterKernel.size() != partOfImage.size()){
        throw std::invalid_argument( "Vector sizes do not equal" );
    }

    float multiplicationResult[3] = {0.0, 0.0, 0.0}; //rgb
    for (uint index = 0; index < filterKernel.size(); index++) {
        for (uint i = 0; i < 3; i++) {
            float pix = static_cast<float>(partOfImage[index][i]);
            multiplicationResult[i] = multiplicationResult[i] +
                                      (filterKernel[index] * pix);
        }
    }


    Colour finalResult = {static_cast<int>(coefficient * multiplicationResult[0]),
                          static_cast<int>(coefficient * multiplicationResult[1]),
                          static_cast<int>(coefficient * multiplicationResult[2])};

    return finalResult;

}