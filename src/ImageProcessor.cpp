//
// Created by mat on 14.08.18.
//

#include "ImageProcessor.h"

ImageProcessor::PictureNumberWithPixelValueArray
ImageProcessor::CreateSharpMapFromGroupOfImages(const std::vector<Image>& originalImages){

    if (originalImages.size()<1){
        throw std::invalid_argument( "There are no pictures to be processed");
    }

    Image originalImage;

    PictureNumberWithPixelValueArray finalTable;
    PictureNumberWithPixelValueArrayRow finalTableRow;

    uint numberOfRows = originalImages[0].getNumberOfRows();
    uint numberOfColumns = originalImages[0].getNumberOfColumns();

    for (uint row = 0; row < numberOfRows; row++) {
        finalTable.push_back(finalTableRow);
        for (uint col = 0; col <  numberOfColumns; col++) {
            finalTable[row].push_back(std::make_pair(originalImages.size()-1, 0));
        }
    }

    for (uint pictureNumber = 0; pictureNumber <  originalImages.size(); pictureNumber++){
        std::cout << "Image number " << pictureNumber << std::endl;

        originalImage = originalImages[pictureNumber];

        Image laplaceImage = GetLaplaceImageFromOriginal(originalImage);

        FindGreaterValueOfGradient(laplaceImage, finalTable, pictureNumber);
    }

    return finalTable;
}

Image ImageProcessor::GetLaplaceImageFromOriginal(Image originalImage){

    Image gaussianImage = PerformGaussBlurFiltering(originalImage);

    gaussianImage.TransformToGrey();

    Image laplaceImage = PerformLaplacianFiltering(gaussianImage);

    laplaceImage.convertToAbs();

    return laplaceImage;
}


void ImageProcessor::FindGreaterValueOfGradient(const Image& processedImage,
                                                PictureNumberWithPixelValueArray& table, uint pictureNumber)
{
    for (uint row = 0; row < processedImage.getNumberOfRows(); row++){
        for (uint col = 0; col < processedImage.getNumberOfColumns(); col++) {
            int intensityValue = processedImage(row, col).red;
            if (table[row][col].second < intensityValue) {
                table[row][col] = std::make_pair(pictureNumber, intensityValue);
            }
        }
    }
}

Image ImageProcessor::PerformGaussBlurFiltering(const Image &sourceImage){

    Image outputImage = sourceImage;
    DiscreteFilter gaussianFilter(gaussianKernel, 1.0/16.0);

    for (uint rowNumber = 0; rowNumber < sourceImage.getNumberOfRows(); rowNumber++){
        for (uint colNumber = 0; colNumber < sourceImage.getNumberOfColumns(); colNumber++){
            outputImage(rowNumber, colNumber) =
                    gaussianFilter * sourceImage.getPixelNeighbourhoodColour(rowNumber, colNumber);
        }
    }
    return outputImage;

}

Image ImageProcessor::PerformLaplacianFiltering(const Image &sourceImage){

    Image outputImage = sourceImage;
    if (sourceImage.isImageIsGrey()){
        outputImage.setImageIsGrey();
    }

    DiscreteFilter laplaceFilter(laplaceKernel, 1.0);

    for (uint rowNumber = 0; rowNumber < sourceImage.getNumberOfRows(); rowNumber++){
        for (uint colNumber = 0; colNumber < sourceImage.getNumberOfColumns(); colNumber++){
            outputImage(rowNumber,colNumber) =
                    laplaceFilter * sourceImage.getPixelNeighbourhoodColour(rowNumber, colNumber);
        }
    }
    return outputImage;

}


Image ImageProcessor::CreateColourImageBasedOnMap(const std::vector<Image>& originalImages,
                                                  const PictureNumberWithPixelValueArray& table)
{
    uint numberOfRows = originalImages[0].getNumberOfRows();
    uint numberOfColumns = originalImages[0].getNumberOfColumns();

    Image finalImage(numberOfRows, numberOfColumns);

    for (uint row = 0; row < numberOfRows; row++) {
        for (uint col = 0; col < numberOfColumns; col++) {
            finalImage(row, col) = originalImages[table[row][col].first](row, col);
        }
    }

    return finalImage;
}

Image ImageProcessor::CreateDepthImageBasedOnMap(const std::vector<Image>& originalImages,
                                                 const PictureNumberWithPixelValueArray& table)
{
    uint numberOfRows = originalImages[0].getNumberOfRows();
    uint numberOfColumns = originalImages[0].getNumberOfColumns();

    Image depthImage(numberOfRows, numberOfColumns);
    depthImage.setImageIsGrey();

    uint numberOfPictures = originalImages.size();
    uint maxIntensityValue = 255;

    uint depthScale = maxIntensityValue / (numberOfPictures);

    for (uint row = 0; row < numberOfRows; row++) {
        for (uint col = 0; col < numberOfColumns; col++) {
            uint picture = table[row][col].first;
            int intensityValue = table[row][col].second;
            int value = 0;
            if (intensityValue > intensityThreshold) {
                value = maxIntensityValue - picture * depthScale;
            }
            depthImage(row, col).red = value;
        }
    }

    return depthImage;
}