//
// Created by mat on 01.08.18.
//

#include <iostream>
#include <opencv2/opencv.hpp>
#include <glob.h>
#include <cstdint>

#include "Image.hpp"

#include "ImageProcessor.h"
#include <sys/types.h>

int main(int argc, char** argv) {

    if (argc != 2) {
        std::cerr << "You should give the path to pictures " << std::endl;
        std::cerr << "./tooploox path/to/files/" << std::endl;
        return -1;
    }

    cv::Mat originalImageCVMat;
    std::vector<Image> originalImages;

    std::string inputFilePath = argv[1];

    glob_t listOfPictures;
    std::string pathWithSuffixOfName = inputFilePath + std::string("*croppped.png");
    glob(pathWithSuffixOfName.c_str(), GLOB_TILDE, NULL, &listOfPictures);

    if (listOfPictures.gl_pathc == 0) {
        std::cerr << "There are no pictures in the indicated directory!" << std::endl;
        return 1;
    }

    for (unsigned int i = 0; i < listOfPictures.gl_pathc; ++i) {
        originalImageCVMat = cv::imread(listOfPictures.gl_pathv[i], 1);
        Image image(originalImageCVMat);
        originalImages.push_back(image);
    }

    ImageProcessor imageProcessor;
    ImageProcessor::PictureNumberWithPixelValueArray sharpMapFromGroupOfImages;

    try
    {
        sharpMapFromGroupOfImages = imageProcessor.CreateSharpMapFromGroupOfImages(originalImages);

        Image focusedImage =
                imageProcessor.CreateColourImageBasedOnMap(originalImages, sharpMapFromGroupOfImages);

        Image depthImage =
                imageProcessor.CreateDepthImageBasedOnMap(originalImages, sharpMapFromGroupOfImages);

        focusedImage.displayImage("Focused image");
        depthImage.displayImage("Depth image");

        focusedImage.saveImage(inputFilePath+"focusedImage.png");
        depthImage.saveImage(inputFilePath+"depthImage.png");
    }
    catch (std::invalid_argument iaex)
    {
        std::cout << "Caught an error!" << iaex.what() <<  std::endl;
    }

    return 0;
}
